<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@include file="/include/header.jspf"%>
<%@include file="/include/navbar.jspf"%>


	<div id="special-offser" class="parallax pricing">
		<div class="container inner">

			<h2 class="section-title text-center">Paiement table</h2>		
			
			<div class="row">

			<div class="col-md-6 col-md-offset-4">			

				<div class="pricing-item">

					<a href="#"><img class="img-responsive img-thumbnail"
						src="img/dish/burger_avocat_bacon.jpg" alt=""></a>

					<div class="pricing-item-details">

						<h3>
							<a href="#">Burger avocat bacon</a>
						</h3>

<!-- 							<a class="btn btn-danger" href="#">Order now</a> -->
						<div class="clearfix"></div>
					</div>
					<!--price tag-->
					<span class="hot-tag br-red">13 €</span>
					<div class="clearfix"></div>
				</div>
				
				<div class="pricing-item">

						<a href="#"><img class="img-responsive img-thumbnail"
							src="img/dish/cake_banane_chocolat.jpg" alt=""></a>

						<div class="pricing-item-details">

							<h3>
								<a href="#">Cake banane chocolat</a>
							</h3>

<!-- 							<a class="btn btn-danger" href="#">Order now</a> -->
							<div class="clearfix"></div>
						</div>
						<!--price tag-->
						<span class="hot-tag br-red">6 €</span>
						<div class="clearfix"></div>
					</div>
					
				
			</div>
			<h2 class="text-center"><span class="col-md-12 label label-primary">Total à payer : 19 €</span></h2>				
			
			<button type="submit" class="col-md-3 btn btn-danger pull-right" style="margin-top:15px">Payer</button>					
				
			</div>
		</div>
		<!-- /.container -->
	</div>
	<!-- /#special-offser -->
	

<%@include file="/include/footer.jspf"%>
	