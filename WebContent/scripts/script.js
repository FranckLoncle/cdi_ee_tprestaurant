$(document).ready(function() {
	$("#listeAnimateurs").hide();
	$("#listeStagiaires").hide();
	$("#listeInscriptions").hide();

	$("#listeAnimateursTitre").click(function() {
		$("#listeAnimateurs").fadeToggle();
	});

	$("#listeStagiairesTitre").click(function() {
		$("#listeStagiaires").fadeToggle();
	});

	$("#listeInscriptionsTitre").click(function() {
		$("#listeInscriptions").fadeToggle();
	});

	$("#jumbotron").hide().fadeIn(1000);
});

var nbBatons = 16;
var joueur1;
var joueur2;
var CompteurTemp = 3;
var joueurSelectionne;

function StartJeuBaton() {
	$("#jeuBaton").empty();
	nbBatons = 16;

	while (joueur1 == null || joueur2 == null) {
		joueur1 = prompt("Nom du joueur 1:");
		joueur2 = prompt("Nom du joueur 2:");
	}
	joueurSelectionne = joueur1;

	$("#enTete").text(joueur1 + " VS " + joueur2);
	$("#jeuBaton").prepend("<h3 id='nbBatons'></h3>");
	$("#nbBatons").text("Batons restants : " + nbBatons);
	$("#jeuBaton").prepend(
			"<div class='alert alert-info' role='alert' id='joueurEnCours'>A " + joueur1
					+ " de jouer (vous pouvez encore jouer " + CompteurTemp
					+ " batons)</div>");
	for (i = 1; i <= 16; i++) {
		$("#jeuBaton").append(
				"<span id='baton" + i
						+ "' onclick='JouerBaton(baton" + i
						+ ")'><img data-src='holder.js/200x200' class='img-thumbnail' width='35' src='img/baton.png'/></span>");
	}
	
	$("#btnPasserLaMain").removeAttr('disabled');
}

function JouerBaton(idBaton) {
	$(idBaton).hide();
	nbBatons--;
	CompteurTemp--;
	$("#nbBatons").text("Batons restants : " + nbBatons);

	if (CompteurTemp == 0) {
		CompteurTemp = 3;
		if (joueurSelectionne == joueur1) {
			joueurSelectionne = joueur2;
		} else {
			joueurSelectionne = joueur1;
		}
	}
	$("#joueurEnCours").text(
			"A " + joueurSelectionne + " de jouer (vous pouvez encore jouer "
					+ CompteurTemp + " batons)");

	if (nbBatons == 0) {
		$("#nbBatons").text("Game Over : " + joueurSelectionne + " a perdu !");
	}
}

function PasserLaMain() {

	if (CompteurTemp < 3) {
		if (joueurSelectionne == joueur1) {
			joueurSelectionne = joueur2;
		} else {
			joueurSelectionne = joueur1;
		}
		CompteurTemp = 3;
		$("#joueurEnCours").text(
				"A " + joueurSelectionne
						+ " de jouer (vous pouvez encore jouer " + CompteurTemp
						+ " batons)");
	}
	else{
		alert("Vous devez jouer au moins un baton !");
	}
}
