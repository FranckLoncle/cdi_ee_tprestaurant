<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@include file="/include/header.jspf"%>
<%@include file="/include/navbar.jspf"%>


	<div id="special-offser" class="parallax pricing">
		<div class="container inner">

			<h2 class="section-title text-center">D�tails de la table</h2>		
			
			<div class="row">
											
					<div class="col-md-6 col-md-offset-4 col-sm-6 col-sm-offset-4">
	
						<div class="pricing-item">
	
							<img class="img-responsive img-thumbnail" src="${requestScope['table'].image}">
	
							<div class="pricing-item-details">
	
								<h3>${requestScope['table'].libelle}</h3>
								<p>${requestScope['table'].nb_place} places</p>
								
								<div class="clearfix"></div>
							</div>
							
							<c:choose>
								<c:when test="${requestScope['table'].statut.id == 1}">
									<span class="hot-tag br-green"></span> 
								</c:when>
								<c:when test="${requestScope['table'].statut.id == 2}">
									<span class="hot-tag br-grey"></span> 
								</c:when>
								<c:when test="${requestScope['table'].statut.id == 3}">
									<span class="hot-tag br-red"></span> 
								</c:when>
								<c:when test="${requestScope['table'].statut.id == 4}">
									<span class="hot-tag br-orange"></span> 
								</c:when>
							</c:choose>
							<div class="clearfix"></div>
						</div>
						
					</div>				

			</div>

		</div>
		
	</div>
	

<%@include file="/include/footer.jspf"%>
	