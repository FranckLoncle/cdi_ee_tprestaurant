<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@include file="/include/header.jspf"%>
<%@include file="/include/navbar.jspf"%>


	<div id="special-offser" class="parallax pricing">
		<div class="container inner">

			<h2 class="section-title text-center">Commandes effectuées</h2>		
			
			<div class="row">

				<c:forEach var="table" items="${requestScope['listeTables']}">
								
					<div class="col-md-6 col-sm-6">
	
						<div class="pricing-item">
	
							<a href="DescriptifTable?idTable=${table.id}"><img class="img-responsive img-thumbnail"
								src="${table.image}" alt=""></a>
	
							<div class="pricing-item-details">
	
								<h3>
									<a href="DescriptifTable?idTable=${table.id}">${table.libelle}</a>
								</h3>
								<p>${table.nb_place} places</p>
								
								<a class="btn btn-danger" href="Paiement?idTable=${table.id}">Payer</a>
									
								<div class="clearfix"></div>
							</div>						
							
							<span class="hot-tag br-orange"></span> 
								
							<div class="clearfix"></div>
						</div>
						
					</div>
								
				</c:forEach>

			</div>

		</div>
		<!-- /.container -->
	</div>
	<!-- /#special-offser -->
	

<%@include file="/include/footer.jspf"%>
	