<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@include file="/include/header.jspf"%>
<%@include file="/include/navbar.jspf"%>

	<div id="header">
		<div class="bg-overlay"></div>
		<div class="center text-center">
			<div class="banner">
				<h1 class="">101 recettes gourmandes</h1>
			</div>
			<div class="subtitle">
				<h4>C'est ça qu' c'est bon !</h4>
			</div>
		</div>
		<div class="bottom text-center">
			<a id="scrollDownArrow" href="#"><i class="fa fa-chevron-down"></i></a>
		</div>
	</div>
	<!-- /#header -->

	<div id="special-offser" class="parallax pricing">
		<div class="container inner">

			<h2 class="section-title text-center">Plats</h2>

			<div class="row">
				<div class="col-md-6 col-sm-6">

					<div class="pricing-item">

						<a href="#"><img class="img-responsive img-thumbnail"
							src="img/dish/burger_avocat_bacon.jpg" alt=""></a>

						<div class="pricing-item-details">

							<h3>
								<a href="#">Burger avocat bacon</a>
							</h3>

<!-- 							<a class="btn btn-danger" href="#">Order now</a> -->
							<div class="clearfix"></div>
						</div>
						<!--price tag-->
						<span class="hot-tag br-red">13 €</span>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="col-md-6 col-sm-6">

					<div class="pricing-item">

						<a href="#"><img class="img-responsive img-thumbnail"
							src="img/dish/cake_banane_chocolat.jpg" alt=""></a>

						<div class="pricing-item-details">

							<h3>
								<a href="#">Cake banane chocolat</a>
							</h3>

<!-- 							<a class="btn btn-danger" href="#">Order now</a> -->
							<div class="clearfix"></div>
						</div>
						<!--price tag-->
						<span class="hot-tag br-red">6 €</span>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="clearfix visible-md"></div>
				<div class="col-md-6 col-sm-6">

					<div class="pricing-item">

						<a href="#"><img class="img-responsive img-thumbnail"
							src="img/dish/chiktaye_morue_guadeloupe.jpg" alt=""></a>

						<div class="pricing-item-details">

							<h3>
								<a href="#">Chiktaye morue guadeloupe</a>
							</h3>

<!-- 							<a class="btn btn-danger" href="#">Order now</a> -->
							<div class="clearfix"></div>
						</div>
						<!--price tag-->
						<span class="hot-tag br-red">13 €</span>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="col-md-6 col-sm-6">

					<div class="pricing-item">

						<a href="#"><img class="img-responsive img-thumbnail"
							src="img/dish/salade_automne_champignons_sautes.jpg" alt=""></a>

						<div class="pricing-item-details">

							<h3>
								<a href="#">Salade automne champignons sautes</a>
							</h3>

<!-- 							<a class="btn btn-danger" href="#">Order now</a> -->
							<div class="clearfix"></div>
						</div>
						<!--price tag-->
						<span class="hot-tag br-red">9 €</span>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="col-md-6 col-sm-6">

					<div class="pricing-item">

						<a href="#"><img class="img-responsive img-thumbnail"
							src="img/dish/salade_chevre_chaud.jpg" alt=""></a>

						<div class="pricing-item-details">

							<h3>
								<a href="#">Salade chevre chaud</a>
							</h3>

<!-- 							<a class="btn btn-danger" href="#">Order now</a> -->
							<div class="clearfix"></div>
						</div>
						<!--price tag-->
						<span class="hot-tag br-red">12 €</span>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="col-md-6 col-sm-6">

					<div class="pricing-item">

						<a href="#"><img class="img-responsive img-thumbnail"
							src="img/dish/taboule_libanais.jpg" alt=""></a>

						<div class="pricing-item-details">

							<h3>
								<a href="#">Taboule libanais</a>
							</h3>

<!-- 							<a class="btn btn-danger" href="#">Order now</a> -->
							<div class="clearfix"></div>
						</div>
						<!--price tag-->
						<span class="hot-tag br-red">8 €</span>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="col-md-6 col-sm-6">

					<div class="pricing-item">

						<a href="#"><img class="img-responsive img-thumbnail"
							src="img/dish/tarte_tatin.jpg" alt=""></a>

						<div class="pricing-item-details">

							<h3>
								<a href="#">Tarte tatin</a>
							</h3>

<!-- 							<a class="btn btn-danger" href="#">Order now</a> -->
							<div class="clearfix"></div>
						</div>
						<!--price tag-->
						<span class="hot-tag br-red">12 €</span>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>

		</div>
		<!-- /.container -->
	</div>
	<!-- /#special-offser -->


	
<%@include file="/include/footer.jspf"%>