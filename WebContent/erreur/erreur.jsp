<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@include file="/include/header.jspf"%>
<%@include file="/include/navbar.jspf"%>
<div class="alert alert-danger col-md-12" role="alert">
	<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
	<span class="sr-only">Erreur:</span>
	<%=request.getAttribute("erreurMessage")%>
</div>
<div>
	<form action="AccueilServlet">
		<button class="btn btn-primary btn-block btn-lg" type="submit">Retour �
			l'accueil</button>
	</form>
</div>
<%@include file="/include/footer.jspf"%>