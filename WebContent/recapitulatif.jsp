<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@include file="/include/header.jspf"%>
<%@include file="/include/navbar.jspf"%>


	<div id="special-offser" class="parallax pricing">
		<div class="container inner">

			<h2 class="section-title text-center">Tables</h2>		
			
			<div class="row">

				<c:forEach var="table" items="${requestScope['listeTables']}">
								
					<div class="col-md-6 col-sm-6">
	
						<div class="pricing-item">
	
							<a href="DescriptifTable?idTable=${table.id}"><img class="img-responsive img-thumbnail"
								src="${table.image}" alt=""></a>
	
							<div class="pricing-item-details">
	
								<h3>
									<a href="DescriptifTable?idTable=${table.id}">${table.libelle}</a>
								</h3>
								<p>${table.nb_place} places</p>
								<c:choose>
									<c:when test="${table.statut.id == 1}">
										<a class="btn btn-danger" href="Reservation?idTable=${table.id}">Reserver</a>
									</c:when>
								<c:otherwise>
									<a class="btn btn-danger" disabled href="Reservation?idTable=${table.id}">Reserver</a>
								</c:otherwise>
								</c:choose>
								<div class="clearfix"></div>
							</div>
							
							<c:choose>
								<c:when test="${table.statut.id == 1}">
									<span class="hot-tag br-green"></span> 
								</c:when>
								<c:when test="${table.statut.id == 2}">
									<span class="hot-tag br-grey"></span> 
								</c:when>
								<c:when test="${table.statut.id == 3}">
									<span class="hot-tag br-red"></span> 
								</c:when>
								<c:when test="${table.statut.id == 4}">
									<span class="hot-tag br-orange"></span> 
								</c:when>
							</c:choose>
							<div class="clearfix"></div>
						</div>
						
					</div>
								
				</c:forEach>

			</div>

		</div>
		<!-- /.container -->
	</div>
	<!-- /#special-offser -->
	

<%@include file="/include/footer.jspf"%>
	