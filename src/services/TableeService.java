package services;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;

import fr.eni.model.Statut;
import fr.eni.model.Tablee;
import fr.eni.utils.JPAUtil;

public class TableeService {
	
	public static List<Tablee> getTables() {
		
		List<Tablee> listeTables = new ArrayList<Tablee>();
		EntityManager em = JPAUtil.getEntityManager();		
		em.getTransaction().begin();		
		
		try {
		    Query q = em.createQuery("SELECT t "
									+ "FROM Tablee t ");

			listeTables = (List<Tablee>) q.getResultList();
			em.getTransaction().commit();		
		} catch (Exception e) {
			System.out.println(e.getMessage());
			em.getTransaction().rollback();
		}

		em.close();
		
		return listeTables;		
	}
	
	public static List<Tablee> getTablesByStatutLibelle(HttpServletRequest request, String param) {
		
		List<Tablee> listeTables = new ArrayList<Tablee>();
		EntityManager em = JPAUtil.getEntityManager();		
		em.getTransaction().begin();
		
		try {
		    Query q = em.createQuery("SELECT t "
									+ "FROM Tablee t "
									+ "INNER JOIN t.statut s "
									+ "WHERE s.libelle = '" + param + "'");

			listeTables = (List<Tablee>) q.getResultList();
			em.getTransaction().commit();		
		} catch (Exception e) {
			System.out.println(e.getMessage());
			em.getTransaction().rollback();
		}

		em.close();
		
		return listeTables;		
	}
	
	
	public static Tablee getTableById(int idTable) {
		
		Tablee table = new Tablee();
		EntityManager em = JPAUtil.getEntityManager();		
		em.getTransaction().begin();
		
		try {
			table = (Tablee) em.find(Tablee.class, idTable);
			em.getTransaction().commit();		
		} catch (Exception e) {
			System.out.println(e.getMessage());
			em.getTransaction().rollback();
		}

		em.close();
		
		return table;		
	}
	
	
	public static void updateStatutTable(Tablee table, Statut statut) {
		
		EntityManager em = JPAUtil.getEntityManager();		
		em.getTransaction().begin();
				
		try {
			System.out.println(statut.toString());
			table.setStatut(statut);
			//em.persist(table);
			em.merge(table);
			
		} catch (Exception e) {
			System.out.println("error TableeService " +e.getMessage());
			em.getTransaction().rollback();
			
		}
		
	}

}
