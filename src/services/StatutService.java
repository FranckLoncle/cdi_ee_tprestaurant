package services;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import fr.eni.model.Statut;
import fr.eni.model.Tablee;
import fr.eni.utils.JPAUtil;

public class StatutService {

	public static Statut getStatutByLibelle(String libelle) {	
		
		Statut statut = new Statut();		
		
		EntityManager em = JPAUtil.getEntityManager();
		em.getTransaction().begin();
		
		try {
		    Query q = em.createQuery("SELECT s "
									+ "FROM Statut s "
									+ "WHERE s.libelle = '" + libelle + "'");

		    statut = (Statut) q.getSingleResult();
			em.getTransaction().commit();		
		} catch (Exception e) {
			System.out.println(e.getMessage());
			em.getTransaction().rollback();
		}

		em.close();
		
		return statut;	
	}
	
	
	
}
