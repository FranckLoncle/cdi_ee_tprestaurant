package services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;

import org.hibernate.mapping.Table;

import fr.eni.model.Client;
import fr.eni.model.Reservation;
import fr.eni.model.Tablee;
import fr.eni.utils.JPAUtil;

public class ReservationService {		
	
	public void reserveTable(HttpServletRequest request, int idTable) {
		
		EntityManager em = JPAUtil.getEntityManager();		
		em.getTransaction().begin();
		
		try {			
			Date date = Calendar.getInstance().getTime();
			
			Client client = new Client();
			client.setNom(request.getParameter("name"));
			client.setTel(request.getParameter("phone"));
			
			em.persist(client);
			
			Tablee table = new Tablee();
			table.setId(idTable);
					
			Reservation reservation = new Reservation();
			reservation.setClient(client);
			reservation.setTable(table);
			reservation.setDate(date);
			reservation.setNb_personne(Integer.parseInt(request.getParameter("size")));
			
		    em.persist(reservation);
			em.getTransaction().commit();	
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			em.getTransaction().rollback();
		}
		em.close();		
	}
	
	public static List<?> getReservation(Tablee table) {		
		
		EntityManager em = JPAUtil.getEntityManager();		
		em.getTransaction().begin();		
		
		List<Object> liste = new ArrayList<Object>();
		
		try {
		    Query q = em.createQuery("SELECT cl, r, rm, m"
									+ " FROM Client cl"
									+ " INNER JOIN cl.reservation r"
									+ " INNER JOIN t.reservationMenu rm"
									+ " INNER JOIN  rm.menu m"
									+ " WHERE r.table.id = " +table.getId()
									+ " AND CONVERT(date, reservation.date) = CONVERT(date, GETDATE())" 
									);
		    
		    liste = q.getResultList();
 
			em.getTransaction().commit();		
		} catch (Exception e) {
			System.out.println(e.getMessage());
			em.getTransaction().rollback();
		}
		em.close();
		
		return liste;		
	}
	
	
	
}
