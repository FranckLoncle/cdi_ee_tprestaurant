package fr.eni.servlet;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.eni.model.Client;
import fr.eni.model.Menu;
import fr.eni.model.Reservation;
import fr.eni.model.ReservationMenu;
import fr.eni.model.Statut;
import fr.eni.model.Tablee;
import fr.eni.utils.JPAUtil;

/**
 * Servlet implementation class PopulateDB
 */
@WebServlet("/PopulateDB")
public class PopulateDB extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PopulateDB() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Date date = Calendar.getInstance().getTime();

		EntityManager em = JPAUtil.getEntityManager();		
		em.getTransaction().begin();		
		
		try {
		
		Statut statut = new Statut();
		statut.setLibelle("libre");
		
		Statut statut2 = new Statut();
		statut2.setLibelle("r�serv�");
		
		Statut statut3 = new Statut();
		statut3.setLibelle("occup�");
		
		Statut statut4 = new Statut();
		statut4.setLibelle("command�");
		
		Tablee table = new Tablee();
		table.setLibelle("Table 1");
		table.setNb_place(5);
		table.setStatut(statut);
		
		Tablee table2 = new Tablee();
		table2.setLibelle("Table 2");
		table2.setNb_place(4);
		table2.setStatut(statut);
		
		Tablee table3 = new Tablee();
		table3.setLibelle("Table 3");
		table3.setNb_place(2);
		table3.setStatut(statut);
	
		Client client = new Client();
		client.setNom("Agnougnagne");
		client.setTel("0622664499");
		
		Client client2 = new Client();
		client2.setNom("Claude Francois");
		client2.setTel("0675789879");
		
		Client client3 = new Client();
		client3.setNom("Tomtom");
		client3.setTel("0622665799");
		
		Reservation reservation = new Reservation();
		reservation.setClient(client);
		reservation.setNb_personne(5);
		reservation.setTable(table);
		reservation.setDate(date);
		
		Reservation reservation2 = new Reservation();
		reservation2.setClient(client2);
		reservation2.setNb_personne(4);
		reservation2.setTable(table2);
		reservation2.setDate(date);

		Reservation reservation3 = new Reservation();
		reservation3.setClient(client3);
		reservation3.setNb_personne(2);
		reservation3.setTable(table3);
		reservation3.setDate(date);
		
		Menu menu = new Menu();
		menu.setLibelle("Menu du jour");
		menu.setPrix(12);
		
		Menu menu2 = new Menu();
		menu2.setLibelle("Menu gastro");
		menu2.setPrix(25);
		
		Menu menu3 = new Menu();
		menu3.setLibelle("Menu bambino");
		menu3.setPrix(8);
		
		// Ajout des menus
		ArrayList<Menu> listeMenus = new ArrayList<Menu>();
		listeMenus.add(menu);
		listeMenus.add(menu2);
		listeMenus.add(menu2);
		listeMenus.add(menu3);
		listeMenus.add(menu3);
	
		reservation.setMenus(listeMenus);
		
		ArrayList<Menu> listeMenus2 = new ArrayList<Menu>();
		listeMenus2.add(menu);
		listeMenus2.add(menu2);
		listeMenus2.add(menu2);
		listeMenus2.add(menu3);
		
		reservation2.setMenus(listeMenus2);
		
		ArrayList<Menu> listeMenus3 = new ArrayList<Menu>();
		listeMenus3.add(menu);
		listeMenus3.add(menu2);
		
		reservation3.setMenus(listeMenus2);
		
		
		// Ajout des r�servations
//		ArrayList<Reservation> listeReservations = new ArrayList<Reservation>();
//		listeReservations.add(reservation);
//		listeReservations.add(reservation2);
//		listeReservations.add(reservation3);
//		
//		Menu menus = new Menu();
//		menus.setReservations(listeReservations);		
		
		em.persist(client);
		em.persist(client2);
		em.persist(client3);
		em.persist(statut);
		em.persist(statut2);
		em.persist(statut3);
		em.persist(statut4);
		em.persist(table);
		em.persist(table2);
		em.persist(table3);
		em.persist(menu);
		em.persist(menu2);
		em.persist(menu3);
		em.persist(reservation);
		em.persist(reservation2);
		em.persist(reservation3);	

		em.getTransaction().commit();			
		em.close();	
		
		} catch (Exception e)
		{
			System.out.println(e.getMessage());
			em.getTransaction().rollback();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		
		
	}


}
