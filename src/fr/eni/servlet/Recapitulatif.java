package fr.eni.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.eni.model.Tablee;
import fr.eni.utils.GestionExceptions;
import services.TableeService;

/**
 * Servlet implementation class Recapitulatif
 */
@WebServlet("/Recapitulatif")
public class Recapitulatif extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Recapitulatif() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
		try {
			RequestDispatcher rd = null;
			
			List<Tablee> listeTables = TableeService.getTables();			
			
			request.setAttribute("listeTables", listeTables);
			rd = getServletContext().getRequestDispatcher("/recapitulatif.jsp");
			rd.forward(request, response);
			
		} catch (Exception e) {
			GestionExceptions.getInstance().getPageErreur(getServletContext(), request, response, e);
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
