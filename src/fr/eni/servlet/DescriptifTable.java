package fr.eni.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.eni.model.Tablee;
import fr.eni.utils.GestionExceptions;
import services.TableeService;

/**
 * Servlet implementation class Recapitulatif
 */
@WebServlet("/DescriptifTable")
public class DescriptifTable extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DescriptifTable() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
		try {
			RequestDispatcher rd = null;
			if(null != request.getParameter("idTable")){
				int idTable = Integer.parseInt(request.getParameter("idTable"));
				Tablee table = TableeService.getTables().get(idTable-1);			
				
				request.setAttribute("table", table);
				rd = getServletContext().getRequestDispatcher("/descriptifTable.jsp");
				rd.forward(request, response);
			}
			else throw new Exception("Identifiant table null");
			
		} catch (Exception e) {
			GestionExceptions.getInstance().getPageErreur(getServletContext(), request, response, e);
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
}
