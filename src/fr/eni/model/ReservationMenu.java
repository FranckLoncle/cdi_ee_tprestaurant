package fr.eni.model;

import javax.persistence.Entity;

public class ReservationMenu {

	private Menu menu;
	private Reservation reservation;

	public Menu getMenu() {
		return menu;
	}

	public void setMenu(Menu menu) {
		this.menu = menu;
	}

	public Reservation getReservation() {
		return reservation;
	}

	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}

}
