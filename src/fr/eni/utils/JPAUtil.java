package fr.eni.utils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;

import org.hibernate.SessionFactory;

public class JPAUtil {
	
	private static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("DemoJPA");//correspond au nom donn� dans persistance.xml
	
	public static EntityManager getEntityManager()
	{
		return emf.createEntityManager();
	}
	public static CriteriaBuilder getCriteriaBuilder()
	{
		EntityManagerFactory emf = 
				Persistence.createEntityManagerFactory("DemoJPA");
		return emf.getCriteriaBuilder();
	}
}
