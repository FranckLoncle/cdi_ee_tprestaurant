package fr.eni.utils;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * G�re les action � mener en cas d'exceptions
 * 
 * @author Franck
 *
 */
public class GestionExceptions extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static GestionExceptions _instance;

	public static GestionExceptions getInstance() {
		return _instance == null ? _instance = new GestionExceptions() : _instance;
	}

	private GestionExceptions() {

	}

	/**
	 * Page appel�e si exception
	 * 
	 * @param request
	 * @param messageErreur
	 * @throws IOException 
	 * @throws ServletException 
	 */
	public void getPageErreur(ServletContext sc, HttpServletRequest request, HttpServletResponse reponse,
			Exception e) throws ServletException, IOException {
		request.setAttribute("titre", "Oups !");
		request.setAttribute("erreurMessage", e.getMessage());
		RequestDispatcher rd = sc.getRequestDispatcher("/erreur/erreur.jsp");
		rd.forward(request, reponse);
	}

}
